#!/bin/bash

set -e

NETWORK=sensei-dev-net
WP_CONTAINER=sensei-dev_wordpress_1

if [ -z "$*" ]
then
	command='/bin/ash'
else
	command="wp $@"
fi

docker run -it --rm --network $NETWORK --volumes-from $WP_CONTAINER wordpress:cli $command
